#!/bin/bash

#Launch optitrack-ros
optitrack-ros &
sleep 2s
echo "optitrack-ros OK"

# Send ROS ACTION to start optitrack-ros
#ART
timeout 5 rosaction call /optitrack/connect '{host: marey, host_port: "1510", mcast: 239.140.93.30, mcast_port: "1511"}'
#PR2
#timeout 5 rosaction call /optitrack/connect '{host: marey, host_port: "1510", mcast: 239.255.42.99, mcast_port: "1511"}'

sleep 2s

#Launch tkbridge
#${HUMUS_TELEKYB_FRAMEWORK}/optitrack/tkbridge/tkbridge.py
roslaunch tkbridge tkbridge.launch node_name:="tkbridge"

#if new objects created run:
# rosservice call /optitrack/refresh
#and relaunch tkbridge.py
