#!/usr/bin/env python
import rospy, math as m
from os.path import basename
from optitrack.msg import or_pose_estimator_state
from geometry_msgs.msg import PoseStamped


class Repub:
    def __init__(self, name):
        print("republishing " 'tkbridge/' + basename(name) +
              ' (from ' + name + ')')
        self.name = name
        self.pub = rospy.Publisher('tkbridge/' + basename(name),
                                   PoseStamped, queue_size=10)
        rospy.Subscriber(name, or_pose_estimator_state, self.callback)

    def callback(self, msg):

        if len(msg.pos) == 0 :
            print ('object '+basename(self.name)+' lost')

        else :
            tk = PoseStamped()

            #tk.header.seq = 
            #tk.header.frame_id = basename(self.name)+'/'+basename(self.name)
            tk.header.frame_id = 'mocap_world'
            tk.header.stamp.secs =  msg.ts.sec
            tk.header.stamp.nsecs =  msg.ts.nsec
            
            tk.pose.position.x = msg.pos[0].x
            tk.pose.position.y = msg.pos[0].y
            tk.pose.position.z = msg.pos[0].z

            tk.pose.orientation.x = msg.pos[0].qx
            tk.pose.orientation.y = msg.pos[0].qy
            tk.pose.orientation.z = msg.pos[0].qz
            tk.pose.orientation.w = msg.pos[0].qw

            self.pub.publish(tk)

def bridge():
    rospy.init_node('tkbridge')

    # get list of topics of 'or_pose_estimator_state' type
    topics = rospy.get_published_topics()
    topics = [t[0] for t in topics if "or_pose_estimator_state" in t[1]]

    # subscribe all topics and republish under the new name/type
    for t in topics:
        r = Repub(t)

    # process events
    while not rospy.is_shutdown():
        rospy.sleep(1.0)

if __name__ == '__main__':
    try:
        bridge()
    except rospy.ROSInterruptException:
        pass

